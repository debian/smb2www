#! /usr/bin/perl

#  This file smbmsg.pl Copyright 1998 Edward Rudd <eddie@omegaware.com>
#  Modifications by Remco van Mook, 1998.


use strict;
use smb2www;

my %all=decode_query;

sub Message_OK {
  header ("$text{message_ok}");
  print "<CENTER>\n";
  print image($cfg{icon_message},"$text{sendmsg}");
  print "<P>\n";
  print href("$all{back}","$text{goback}")."\n";
  trailer;
}

my $key = "";
my $back = "";
foreach $key (keys %all) {
  $all{$key} = urlDecode $all{$key};
}

if ($all{'mesg'} eq '') {
  header ("$text{sendmsg}");
  print "<H1>$text{sendmsg2} ";

  if ( $all{'host'} eq '' ) {
    print "$text{workgroup} $all{group}";
  } else {
    print "$all{host}";
  }
  print ":</H1>\n";
  if ($all{'back'} eq '') {
    $back = "$ENV{'HTTP_REFERER'}";
  } else {
    $back = "$all{'back'}";
  }
  print << "EOF" ;
<CENTER><FORM ACTION=\"$cfg{cgiroot}/smbmsg.pl\" METHOD=get>
<TEXTAREA name="mesg" rows=6 cols=50></TEXTAREA><P>
<INPUT type="submit" value="$text{submit}">
<INPUT type="hidden" name="group" value="$all{group}">
<INPUT type="hidden" name="master" value="$all{master}">
<INPUT type="hidden" name="host" value="$all{host}">
<INPUT type="hidden" name="back" value="$back">
</FORM>
EOF
trailer;

} else {
  if ( $all{host} eq '' ) {
    my @smbout = GetSMBHosts ("$all{master}","$all{group}");

    if ( $#smbout < 0 ) {
      header ("$text{no_group}");
      print "<H1>$text{no_group2}</H1><BR>\n";
      table ( 
      href (shref("all"),image ($cfg{icon_all},"$text{network}")),
      "<H3>$text{ent_net}</H3>",
      ""
      );
      trailer;
      exit;
    }
    my $line = "";
    foreach $line ( @smbout ) {
      SendHostMessage ($line->{name},$all{'mesg'});
    }
    Message_OK;
  } else {
    SendHostMessage ($all{'host'},$all{'mesg'});
    Message_OK;
  }

}
