#! /usr/bin/perl

# SMB2WWW - a smb to WWW gateway; access windows computers through a browser
# Copyright (C) 1997 Remco van Mook

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# The author can be contacted by e-mail: remco@samba.anu.edu.au

use strict;
use smb2www;

select STDOUT; $| = 1;

$ENV{'USER'}=$cfg{username};

my %all=decode_query;
my $key = "";
foreach $key (keys %all) {
  $all{$key} = urlDecode $all{$key};
}

if (($all{auth} eq "") and ($all{user}.$all{pass} ne "")) {
  my $auth = MakeAuth ($all{user},$all{pass});
  $all{auth} = $auth;
  my $url = shref("dir","$all{group}","$all{master}","$all{host}","$all{share}",
                  "$all{dir}","","","","","$auth");
  print << "EOF";
Content-type: text/html

<HTML>
<META HTTP-EQUIV="Refresh" CONTENT="0; URL=$url">
<HEAD>
<TITLE>Reloading for secure URL</TITLE>
</HEAD>
</HTML>
EOF
  exit;
} elsif ($all{auth} ne "") {
  ($all{user},$all{pass}) = GetAuth ("$all{auth}");
}



my @smbout = GetSMBDir ("$all{host}","$all{share}","$all{dir}","$all{user}","$all{pass}");

if ($smbout[0]->{name} eq "") {
   header ("$text{req_pass} $all{share}$all{dir} $text{on_computer} $all{host} $text{or_acc_den}.");
   my $group = urlEncode ($all{group});
   my $master = urlEncode ($all{master});
   my $host = urlEncode ($all{host});
   my $share = urlEncode ($all{share});
   my $dir = urlEncode ($all{dir});
  print << "EOF";
</TABLE>
<CENTER><H1>$text{name_pass}</H1>
<FORM ACTION=\"$cfg{cgiroot}/smbdir.pl\" METHOD=post>
$text{name}: <INPUT type="text" name="user" SIZE="15">
$text{upass}: <INPUT type="password" name="pass" SIZE="15">
<INPUT type="submit" value="$text{submit}">
<INPUT type="hidden" name="group" value="$group">
<INPUT type="hidden" name="master" value="$master">
<INPUT type="hidden" name="host" value="$host">
<INPUT type="hidden" name="share" value="$share">
<INPUT type="hidden" name="dir" value="$dir">
</FORM>
</CENTER></BODY></HTML>
EOF

} else {
   my @tar = split(/\\/,$all{dir});
   my $tarfile = $tar[$#tar].".tar";
   my $tardir = $all{dir};
   if ($tarfile eq ".tar") { 
      $tarfile="$all{share}.tar";
   } 
   header ("$text{content} $all{share}$all{dir} $text{on_computer} $all{host}","norefresh");
   table (href (shref("all"), image ($cfg{icon_all},"$text{network}")), 
          "", 
          "", 
          "<H3>$text{ent_net2}</H3>",
          ""
   );
   if (!($all{group} eq "") or !($all{master} eq "")) {
      table (href (shref("group","$all{group}","$all{master}"),
                   image ($cfg{icon_group},"$text{workgroup}")), 
             "", 
             "", 
             "<H3>$all{group}</H3>", 
             ""
      );
   }
   table (
     href (shref("share","$all{group}","$all{master}","$all{host}"), 
           image ($cfg{icon_computer},"$text{shares}")), 
     "", 
     "", 
     "<H2>$all{host}</H2>", 
     ""
);
   table ("",
          href (shref("dir","$all{group}","$all{master}",
                      "$all{host}","$all{share}","","","","","","$all{auth}"), 
                image ($cfg{icon_share},"$text{top_share}")), 
          "", 
          "<H3><I>$all{share}</I></H3>", 
          "$all{dir}"
   );
   table ("",
          href( shref("tar","","","$all{host}","$all{share}",
                      "/$tardir/$tarfile","","","","","$all{auth}"),
                image ($cfg{icon_archive}, "TAR")),
          "",
          "<H3>".href( shref("tar","","","$all{host}","$all{share}",
                             "/$tardir/$tarfile","","","","","$all{auth}"),
                      "$text{down_dir}")."</H3>",
          ""
   ); 
   my $line = "";         
   foreach $line ( @smbout ) {
      my $url = ""; my $desc = ""; my $image = "";
      if ( $line->{attr} =~ /D/ ) { # Check if it's a directory
        my $dir ="";
        if ( $line->{name} eq "." ) {
          $dir = $all{dir};
        } elsif ( $line->{name} eq ".." ) { 
           if ( $all{dir} =~ /(.+)\\.+$/ ) {
             $dir = $1;
           }
        } else {
          $dir = "$all{dir}\\$line->{name}";
        }
        $url = shref("dir","$all{group}","$all{master}","$all{host}",
                     "$all{share}","$dir",
                     "$all{user}","$all{pass}","","$line->{date}","$all{auth}");
        $image = image ($cfg{icon_dir}, "$text{dir}");
      } else { # Nope, so it must be a file
        $url = shref("file","$all{group}","$all{master}","$all{host}",
                     "$all{share}","$all{dir}\\/$line->{name}","$all{user}",
                     "$all{pass}","$line->{size}","$line->{date}","$all{auth}");
        $image = image ($cfg{icon_file},"$text{file}");
        $desc = "<FONT size=-1>$line->{size}  $line->{date}</FONT>";
      }
      table ("", "", href ($url,$image), href ($url,"$line->{name}"), $desc);
   }
   trailer;
}
