#! /usr/bin/perl

# SMB2WWW - a smb to WWW gateway; access windows computers through a browser
# Copyright (C) 1997 Remco van Mook

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# The author can be contacted by e-mail: remco@samba.anu.edu.au

use strict;
use smb2www;

my %all=decode_query;
my $key = "";
foreach $key (keys %all) {
  $all{$key} = urlDecode $all{$key};
}

$ENV{'USER'}=$cfg{username};

my @smbout = GetSMBShr("$all{'host'}"); 

if ($#smbout < 0 ) { 
   header ("$text{no_host}");
   print "<H1>$text{no_host2}</H1><BR>\n";
   if (($all{'group'} ne "") or
       ($all{'master'} ne "")) {

     table ( href ("$cfg{cgiroot}/smb2www.pl", image ($cfg{icon_all},"$text{network}")),
             "",
             "<H3>".href ("$cfg{cgiroot}/smb2www.pl",'$text{ent_net}')."</H3>",
             "",
             ""
     );
     table ( "",
             href ( shref("group","$all{'group'}","$all{'master'}"), 
                    image ($cfg{icon_group},"$text{workgroup}")),
             "<H3>".href ( shref("group","$all{'group'}","$all{'master'}"),$all{'group'})."</H3>",
             "",
             ""
     );
   } else {   
     table ( href (shref("all"), image ($cfg{icon_all},"$text{network}")),
              "",
              "<H3>$text{ent_net}</H3>"
     ); 
   }
   trailer;

} else {

   header ("$text{shares_on} $all{'host'}");
   table ( href ( shref("all"), image ($cfg{icon_all},"$text{network}")),
           "",
           "",
           "<H3>$text{ent_net}</H3>",
           ""
   );
   if ($all{'group'}) {
     table ( href ( shref ("group","$all{'group'}","$all{'master'}"),
                 image ($cfg{icon_group},"$text{workgroup}")),
             "",
             "",
             "<H3>$all{'group'}</H3>",
             ""
     );
   } 
   table ( "", 
           image ($cfg{icon_computer},"$text{shares}"),
           "", 
           "<H3>$all{'host'}</H3>",
           ""
   );
   table ( "",
           "",
           href (shref("msg","$all{'group'}","$all{'master'}","$all{'host'}"),
                 image ($cfg{icon_message},"$text{sendmsg}")),
           "",
           "$text{sendmsg}"
   );

   table ( "<FORM ACTION=\"$cfg{cgiroot}/smbdir.pl\" METHOD=GET>",
            "<INPUT TYPE=\"hidden\" NAME=\"group\" VALUE=\"$all{'group'}\">".
            "<INPUT TYPE=\"hidden\" NAME=\"master\" VALUE=\"$all{'master'}\">".
            "<INPUT TYPE=\"hidden\" NAME=\"host\" VALUE=\"$all{'host'}\">",
           "<INPUT TYPE=\"image\" BORDER=0 SRC=\"$cfg{imgroot}/$cfg{icon_share}\">",
           "<INPUT TYPE=\"text\" name=\"share\" SIZE=\"15\">",
           "</FORM>\n"
         );

   my $line = "";
   foreach $line ( @smbout ) {
     if ( $line->{type} eq "Disk" ) {
       my $url = shref("dir","$all{'group'}","$all{'master'}","$all{'host'}",
                     "$line->{name}","");
       table ( "",
               "",
               href ($url,image ($cfg{icon_share},"$text{share}")),
               href ($url,"<B>$line->{name}</B>"),
               "$line->{comment}"
       );
     } elsif ( $line->{type} eq "Printer") {
       table ( "",
               "",
               image ($cfg{icon_printer},"$text{printer}"),
               "<B>$line->{name}</B>",
               "$line->{comment}"
       );
     }
   }
   trailer;
}
