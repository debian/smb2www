#! /usr/bin/perl

use smb2www;


%helptext = ();
open (HELPTEXT,"$cfg{cfgdir}/$cfg{language}_help.lang") or die "SMB2WWW: No language support found for $cfg{language} help";
while (<HELPTEXT>) {
  if ( $_ =~/([\w\_]+)\s*=\s*(.+)/ ) {
     $helptext{$1} = $2;
  }
}
close HELPTEXT;



header ($helptext{header},"norefresh");

print "</TABLE>\n";
print "<H1>$helptext{header}</H1>\n";
print "<BR>\n";
print $helptext{info}."\n";
print "<BR>\n";
print $helptext{info2}."\n";
print "<BR>\n";
print "<TABLE>\n";
table ( image ($cfg{icon_other}, "$text{othernet}"),
        "$helptext{othernet}",
        ""
);
table ( image ($cfg{icon_all},"$text{network}"),
        "$helptext{network}",
        ""
);
table ( image ($cfg{icon_group},"$text{workgroup}"),
        "$helptext{workgroup}",
        ""
);
table ( image ($cfg{icon_computer},"$text{shares}"),
        "$helptext{shares}",
        ""
);
table ( image ($cfg{icon_share},"$text{share}"),
        "$helptext{share}",
        ""
);
table ( image ($cfg{icon_dir},"$text{dir}"),
        "$helptext{dir}",
        ""
);
table ( image ($cfg{icon_file},"$text{file}"),
        "$helptext{file}",
        ""
);
table ( image ($cfg{icon_archive}, "TAR"),
        "$helptext{tar}",
        ""
);
table ( image ($cfg{icon_help}, "$text{help}"),
        "$helptext{help}",
        ""
);
my $referer=$ENV{'HTTP_REFERER'} || "$cfg{cgiroot}/smb2www.pl";
print "</TABLE>\n";
print "<CENTER>".href("$referer", image($cfg{icon_all}, "$text{ent_net}"))."<H3>$text{goback}</H3></CENTER>";
print "<HR WIDTH=\"75%\">\n";
print "<CENTER>Comments/remarks/kudos to:<i>remco\@samba.anu.edu.au</i></CENTER>\n";
print "<H6>Language support for $cfg{language} created by $text{lang_author}</H6>\n";
print "</BODY></HTML>\n";
