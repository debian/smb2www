#! /usr/bin/perl

# SMB2WWW - a smb to WWW gateway; access windows computers through a browser
# Copyright (C) 1997 Remco van Mook

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# The author can be contacted by e-mail: remco@samba.anu.edu.au

use strict;
use smb2www;

my @smbout = GetSMBGroups;

header ("$text{welcome}");
table ( 
  href ($cfg{linkto_other}, image ($cfg{icon_other},"$text{othernet}")), 
  "",
  href ($cfg{linkto_other}, "<H2><DIV ALIGN=right>$text{othernet}</DIV></H2>")
);

table ( 
  "<FORM ACTION=\"$cfg{cgiroot}/smbshr.pl\" METHOD=GET>".
    "<INPUT TYPE=\"image\" BORDER=0 SRC=$cfg{imgroot}/$cfg{icon_computer}>",
  "",
  "<DIV ALIGN=right><INPUT TYPE=\"text\" name=\"host\" SIZE=\"15\"></DIV>".
    "<INPUT TYPE=\"hidden\" NAME=\"group\" VALUE=\"\">".
    "<INPUT TYPE=\"hidden\" NAME=\"master\" VALUE=\"\">".
    "</FORM>\n"
);


my $line = "";
foreach $line ( @smbout ) { 
  my $url = shref ("group","$line->{name}","$line->{master}"); 
  table ( 
    "",
    href ($url,image ($cfg{icon_group},"$text{workgroup}")),
    "<DIV ALIGN=right>".href($url,"<B>$line->{name}</B>")."</DIV>"
  );
}
trailer;
