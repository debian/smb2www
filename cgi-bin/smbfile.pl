#! /usr/bin/perl

# SMB2WWW - a smb to WWW gateway; access windows computers through a browser
# Copyright (C) 1997 Remco van Mook

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# The author can be contacted by e-mail: remco@samba.anu.edu.au

use strict;
use smb2www;

sub URLParse {
  my ($host, $share, $file, $user, $pass) = @_;
  $file =~ s/^(.*)\/([^\/]*)$/$1$2/ ;
  if ( $user ne "" ) { $user="-U$user"; }
  if ( $pass ne "") {
    if ( $user eq "" ) { $user = "-Uguest" }
  } else { $pass = "-N" }
  my @input = `$cfg{bindir}/smbclient //$host/$share $pass $user -d2 -c "get \"$file\" -"`;
  my $url = $input[1];
  chop $url;
  if ( $url =~ /URL\=(.*)/ ) {
    print "Location: $1\n\n";
  } else { 
  # Something odd in a .url file, so just throw it at the client, YMMV
    print "Content-type: application/octet-stream\n\n";
    foreach (@input) { print $_}
  }
}

select STDOUT; $| = 1;
$ENV{'USER'}=$cfg{username};

my $info = $ENV{'PATH_INFO'};
my @muddy="";
my $filesize=""; my $filename = "";
my %all=decode_query;
my $key = "";
foreach $key (keys %all) {
  $all{$key} = urlDecode $all{$key};
}

if ($all{auth} ne "") {
  ($all{user},$all{pass}) = GetAuth ("$all{auth}");
}

if ($info =~ /\/([\w\s\$\#\!\-\_\%\'\.\+]+)\/([\w\s\$\#\!\-\_\%\'\.\+]+)\/([\d]+)\/(.*)/) {
   $all{host} = $1;
   $all{share} = $2;
   $filesize= $3;
   $filename = $4;
}

my $ext="";
if ($filename =~ /([^\/]+)\/(.*)/ ) {
   $ext = (split (/\./ , $2))[-1]; 
}

# If the file extension is something special, like .url or .lnk, do something
# intelligent with it. At the moment, only .url is supported.

if ($ext eq "url") {
  URLParse ("$all{host}","$all{share}","$filename","$all{user}","$all{pass}");
} else {
  if ($all{lm} ne "") { print "Last-Modified: ".httptime("$all{lm}")."\n" }
  print "Pragma: no-cache\n";
  if ( $filesize ne "") { print "Content-Length: $filesize\n" }
  print "Content-type: ".mimetype ($ext)."\n\n";
  GetSMBFile ("$all{host}","$all{share}","$filename","$all{user}","$all{pass}");
}
