use POSIX;

$is_root=1 if ($> == 0 );

open (VERSION, "VERSION");
$version=<VERSION>;
chop $version;
close VERSION;

print <<"EOF";

Welcome to the installation of SMB2WWW, release $version. 
Press <Return> to continue, or press <Ctrl-C> to stop.
EOF

$foo=<STDIN>;


if ($is_root == 1) {
  while (! getpwnam($uname)) {
    print "$uname is not a valid username.\n" if (! $uname eq ""); 
    print "What username shall I use to install smb2www [www-data] ? ";
    $uname=<STDIN>;
    chop $uname;
    $uname = "www-data" if ($uname eq "");
  }

  while (! getgrnam($gname)) {
    print "$gname is not a valid groupname.\n" if (! $gname eq "");
    print "What groupname shall I use to install smb2www [www-data] ? ";
    $gname= <STDIN>;
    chop $gname;
    $gname = "www-data" if ($gname eq "");
  }
}

print "Where shall I put smb2www [/usr/local/smb2www] ? ";
$dir=<STDIN>; chop $dir;
if ( $dir eq "") { $dir = "/usr/local/smb2www" };
print "\n";
die "Directory already exists ! Move first before continuing !" if ( -d $dir);
mkdir "$dir",0755 or die "Unable to create directory ! Check your permissions.";

print "\nDirectory $dir created successfully.\nNow creating subdirectories.\n";

mkdir "$dir/images",0755 or die "$!\n";
mkdir "$dir/cgi-bin",0755 or die "$!\n";
mkdir "$dir/bin",0755 or die "$!\n";
mkdir "$dir/etc",0755 or die "$!\n";

print "Subdirectories bin, etc, images and cgi-bin successfully created.\n\n";
if ($is_root) {
  print "Setting ownership to $uname:$gname...";
  $uid=getpwnam($uname);
  $gid=getgrnam($gname);
  chown $uid,$gid,$dir,"$dir/images","$dir/cgi-bin","$dir/bin","$dir/etc" or die "\n$!\n";
  print "done.\n\n";
}

$smbc = "";
while (not -x "$smbc/smbclient" ) {
  print "In which directory is smbclient located [/usr/local/samba/bin] ? ";
  $smbc=<STDIN>; chop $smbc;
  if ( $smbc eq "") { $smbc = "/usr/local/samba/bin" }; 
  print "\n";
}

symlink "$smbc/smbclient","$dir/bin/smbclient" or die "unable to symlink smbclient to $dir/bin !\n";

$perl = "";
while (not -x "$perl" ){
  print "What is your perl 5 binary [/usr/bin/perl] ? ";
  $perl=<STDIN>; chop $perl;
  if ( $perl eq "") { $perl = "/usr/bin/perl" };
  print "\n";
}

print "Which server shall I use as master browser ? ";
$mbrowse=<STDIN>; chop $mbrowse;
print "\n";

srand ( time ^ ($$ + ($$ << 15)) );
while (length $key < 25) {
  $key = $key.chr(rand(64)+64); 
}
print "Which language would you like to use with SMB2WWW ?\n   (english dutch finnish) [english] ";
$language = <STDIN>; chop $language;
if ( $language eq "" ) { $language = "english" };
print "\n";



print "Which path shall I use for SMB2WWW pictures on your webserver\n[/smbimg] ? ";
$imgpath=<STDIN>; chop $imgpath;
if ( $imgpath eq "" ) { $imgpath = "/smbimg" };
print "\n";	

print "Which path shall I use as SMB2WWW root on your webserver [/samba] ? ";
$cgiroot=<STDIN>; chop $cgiroot;
if ( $cgiroot eq "" ) { $cgiroot = "/samba" };
print "\n";

print "Where is your mime.types file [/etc] ? ";
$mime=<STDIN>; chop $mime;
if ( $mime eq "" ) { $mime = "/etc" };
$mime= $mime."/mime.types";
print "\n";

print "Now modifying scripts and configuration files...\n\n";

print "To make modifications later, edit $dir/etc/smb2www.conf\n";

# smb2www.conf

open OUT, ">$dir/etc/smb2www.conf" or die "unable to create file $dir/etc/smb2www.conf !\n"; 

print OUT <<"EOF";
version       = $version
masterbrowser = $mbrowse
username      = nobody
refresh       = 300
cache         = max-age = 300
bindir        = $dir/bin
cfgdir        = $dir/etc
imgroot       = $imgpath
cgiroot       = $cgiroot
linkto_other  = http://samba.anu.edu.au/samba/smb2www/
background    = cloud.gif
icon_other    = world.gif
icon_all      = network.gif
icon_group    = group.gif
icon_computer = computer.gif
icon_share    = share.gif
icon_printer  = printer.gif
icon_dir      = folder.gif
icon_file     = file.gif
icon_archive  = archive.gif
icon_help     = help.gif
icon_message  = message.gif
link          = #7f0000
vlink         = #4f0000
mimetype      = $mime
language      = $language
key           = $key
EOF

close OUT;

# All .pl files

@pl = ("smbfile.pl","smbtar.pl","smbhelp.pl","smbshr.pl",
       "smbdir.pl","smbgrp.pl","smb2www.pl","smbmsg.pl");

foreach $plfile ( @pl) {
  open IN, "cgi-bin/$plfile";
  $pline = <IN>;
  open OUT, ">$dir/cgi-bin/$plfile" or die "$!\n";
  print OUT "#! $perl\n";
  while (<IN>) {
    print OUT $_;
  }
  close OUT;
  chmod 0550,"$dir/cgi-bin/$plfile"; 
  if ($is_root) { chown $uid,$gid, "$dir/cgi-bin/$plfile" }
  close IN;
}

# smb2www.pm

open IN, "cgi-bin/smb2www.pm";
open OUT, ">$dir/cgi-bin/smb2www.pm";

while (<IN>) {
  $_ =~ s#/usr/local/smb2www/etc/smb2www.conf#$dir/etc/smb2www.conf#;
  print OUT $_;
}
close OUT;
if ($is_root) { chown $uid, $gid, "$dir/cgi-bin/smb2www.pm" }
close IN;

# copy all images

`cp -v images/* $dir/images/`;
if ($is_root) { 
   opendir IMGDIR, "$dir/images";
   @allfiles = grep !/^\./, readdir IMGDIR;
   closedir IMGDIR;
   foreach $file (@allfiles) {
     chown $uid,$gid, "$dir/images/$file"; 
   }
}

# copy all language files

`cp -v etc/*.lang $dir/etc/`; 
if ($is_root) { 
  opendir ETCDIR, "$dir/etc";
  @allfiles = grep !/^\./, readdir ETCDIR;
  closedir ETCDIR;
  foreach $file (@allfiles) {
    chown $uid, $gid, "$dir/etc/$file"
  }
}

# Setting permissions

chmod 0550,"$dir/cgi-bin";

print "\nAdd this to your apache config:\n";
print "-------------------------------\n\n";
print "Alias $imgpath $dir/images\n";
print "ScriptAlias $cgiroot/ $dir/cgi-bin\n";

print "\nThank you for using SMB2WWW.\n\n";
